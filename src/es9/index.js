//Estas son las principales caracteristicas de ES9

//***** Destructurar *****// 
//Como se destructura a estos elementos
const obj = {
  name: 'Gianni',
  age: 28,
  country: 'MX'
};

let { country, ...all } = obj;
console.log(all);

//***** Unir elemento *****// 
//Unir elementos de un objeto a otro objeto
const obj = {
  name: 'Gianni',
  age: 28
}

const obj2 = {
  ...obj,
  country: 'MX'
}

console.log(obj2);

//***** Promise.finally *****// 
const helloWorld = () => {
  return new Promise((resolve, reject) => {
    (true)
      ? setTimeout(() => resolve('Hello World'), 3000)
      : reject(new Error('Test Error'))
  });
};

helloWorld()
  .then(response => console.log(response))
  .catch(error => console.log(error))
  .finally(() => console.log('Finalizo')) //Es como en java, ejecutar algo cuando todo haya terminado aunque haya o no un error


//***** Regex *****// 
const regexData = /([0-9]{4})-([0-9]{2})-([0-9]{2})/;
const match = regexData.exec('2018-04-28');
const year = match[1];
const month = match[2];
const day = match[3];
console.log('Date -> ', year, month, day);