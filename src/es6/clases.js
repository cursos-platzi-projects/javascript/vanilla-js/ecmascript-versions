class calculator {
  constructor() {
    this.valueA = 0;
    this.valueB = 0;
  }
  //Metodo sum
  sum(valueA, valueB) {
    this.valueA = valueA;
    this.valueB = valueB;
    return this.valueA + this.valueB;
  }
}

const calc = new calculator();
console.log(calc.sum(2,2));

import hello from './module';

hello();

//***** Generadores *****//
//El asterisco significa que es una funcion generator
function* helloWorld() {
  if(true) {
    yield 'Hello, '; //yield significa que va a retornar algo
  }

  if(true) {
    yield 'World';
  }
};

const generatorHello = helloWorld();
console.log(generatorHello.next().value);
console.log(generatorHello.next().value);
console.log(generatorHello.next().value);//Para ver que sucede

//En conclusion, esto me suena a que se retornar varios valores en vez de que sea solo uno con el return.