//***** Parametros en objectos *****//
let name = 'Gianni';
let age = 28;

//ES5
obj = { name: name, age: age };

//ES6
obj2 = { name, age};
console.log(obj2);

//***** Arrow function *****//
const names = [
  { name: 'person 1', age: 28},
  { name: 'person 2', age: 82}
];

let listOfNames = names.map(function (item) {
  console.log(item.name);
});

let listOfNames2 = names.map(item => console.log(item.name) );

const listOfNames3 = (name, age, country) => {
  //codigo ...
}

const listOfNames4 = name => {
  //codigo ...
}

const square = num => num * num;

//***** Promesas *****//
//Una promesa es algo que esperas que va a pasar no importa si hay error o no
const helloPromise = () => {
  return new Promise((resolve, reject) => {
    if(false) {
      resolve('Resultado');
    }
    else {
      reject('Error');
    }
  });
}

helloPromise()
  .then(response => console.log(response)) //Cuando el resultado fue positivo
  .then(() => console.log('hola')) //Se pueden anidar varios elementos then
  .catch(error => console.log(error)) //Cuando hubo un error