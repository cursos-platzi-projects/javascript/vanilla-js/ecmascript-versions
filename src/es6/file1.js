//**** Multilinea *****//
//Antes
let lorem = "Hola, esta es la primer frase \n"+
            "Esta es la segunda frase";
        
//Con ES6
//Usando los template se puede hacer el multilinea con solo dar un salto de linea
let lorem2 = `Hola, esta solo es la primera frase,
y la segunda frase
    `;            

//***** Desestructuracion *****//    
let person = {
  'name': 'Gianni',
  'age': 28,
  'country': 'MX'
};

//Antes
console.log(person.name, person.age);

//Con ES6
let {name, age, country } = person;
console.log(name, age, country);

//***** Spread Operator *****//  
let team1 = ['Person 1', 'Person 2', 'Person 3'];
let team2 = ['Person 4', 'Person 5', 'Person 6'];

let education = ['Gianni', ...team1, ...team2];