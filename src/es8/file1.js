const data = {
  frontend: 'Karen',
  backend: 'Gianni',
  design: 'Caro'
}

const entries = Object.entries(data);
console.log(entries)

//Cuantos elementos contine nuestro arreglo
console.log(entries.length)

const data2 = {
  frontend: 'Karen',
  backend: 'Gianni',
  design: 'Caro',
}

//Obtener solo los values del objeto
const values = Object.values(data2)
console.log(values)
console.log(values.length)

//***** Pad *****// 
const string = 'hello';
console.log(string.padStart(7, 'hi'))
console.log(string.padEnd(12, '-----'))

//Los trailing commas que ya no dan error si hay una coma al final de una propiedad
const obj = {
  name: 'Gianni', //Establece que puede haber un valor como tambien no puede haber y no pasa nada
}